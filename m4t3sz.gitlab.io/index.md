whoami
======

Hello everyone I'm M4t35Z!

*--Just a 19 yo boy interested in Cybersecurity--*

I'm trying to do do my best to be better every day and learn as much as I can.  
I play [HackTheBox](https://hackthebox.eu) and [TryHackMe](https://tryhackme.com). I'm also trying out Bug Bounties :D  
I'm the co-leader of a hungarian capture the flag team [1337B01S](https://ctftime.org/team/107200).

I use debian as my main OS. I've a custom [dwm](https://dwm.suckless.org) setup which I can port to every other linux distro / BSD systems.

In this website, I will release writeups for ended CTF's, HackTheBox and TryHackMe boxes. You can also find some of my projects, tools in the [Showcase](../showcase/) section (revshellgen is awesome give it a try :D).

Oh yeah I also play games. You can find me on CS:GO, League of Legends, GTA5 and Minecraft.  
I'm a big fan of retro games like DOOM1, DOOM2, Wolfenstein3D and Duke Nukem 3D!

If you want to play some games or discuss something just DM me on discord!

Some of my profiles:
- [HackerOne](https://hackerone.com/m4t35z)
- [HackTheBox](https://www.hackthebox.eu/profile/112519)
- [TryHackMe](https://tryhackme.com/p/M4t35Z)
- Discord: `\m4t#6669`
