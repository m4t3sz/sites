<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>M4t35Z's Blog - basic edition</title>
	<link rel="stylesheet" type="text/css" href="https://m4t3sz.gitlab.io/bsc/style.css"/>
</head>

<div id="header">
	<a id="headerLink" href="//m4t3sz.gitlab.io/bsc/">m4t3sz.gitlab.io/bsc</a>
	<span> - </span>
	<span id="headerSubtitle">M4t35Z's Blog - basic edition</span>
</div>
<hr />
<div id="menu">
	<a href="//m4t3sz.gitlab.io/"><b>home</b></a>
	<a href="//twitter.com/szilak44/">twitter</a>
	<a href="//github.com/matesz44/">github</a>
	<a href="//github.com/matesz44/dotfiles/">dotfiles</a>
	<a href="//github.com/matesz44/scripts/">scripts</a>
	<span class="right">
		<a href="//gitlab.com/m4t3sz/sites/">source</a>
	</span>
</div>
<hr />
<div id="content">
<div id="nav">
	<ul>
	<li><a href="/bsc">about</a></li>
	<li><a href="//m4t3sz.gitlab.io/bsc/showcase/">showcase/</a></li>
	<li><a href="//m4t3sz.gitlab.io/bsc/writeup/"><b>>writeup/</b></a>
		<ul>
		<li><a href="//m4t3sz.gitlab.io/bsc/writeup/bug/"><b>>bug/</b></a>
			<ul>
			<li><a href="//m4t3sz.gitlab.io/bsc/writeup/bug/keybase-path-traversal/"><b>>keybase path traversal/</b></a></li>
			</ul>
		</li>
		<li><a href="//m4t3sz.gitlab.io/bsc/writeup/ctf/">ctf/</a></li>
		<li><a href="//m4t3sz.gitlab.io/bsc/writeup/htb/">htb/</a></li>
		<li><a href="//m4t3sz.gitlab.io/bsc/writeup/thm/">thm/</a></li>
		</ul>
	</li>
	</ul>
</div>
<hr />
<div id="main">

<blockquote><p>2021.09.22</p>
</blockquote>
<h1>Keybase allows \ (backslashes) in uploaded filenames which leads to path traversal in windows and it can be chained to rce</h1>
<p><img src="path-trav-confirmed.png" alt="path-trav-confirmed.png" /></p>

<p>
<video width=100% controls>
<source src="rce-via-path-trav.mp4" type="video/mp4">
</video> 
<a href="rce-via-path-trav.mp4">rce-via-path-trav.mp4</a></p>

<h2>Stats</h2>
<pre><code>Reported to: Keybase
Severity: High
Weakness: Path Traversal
Bounty: $2000
CVE ID: CVE-2021-34422
</code></pre>
<ul>
<li><a href="https://nvd.nist.gov/vuln/detail/CVE-2021-34422">CVE-2021-34422</a></li>
<li><a href="https://explore.zoom.us/en/trust/security/security-bulletin">Zoom's security Bulletin</a></li>
</ul>
<p><img src="mention.png" alt="mention.png" /></p>
<h2>Backstory</h2>
<p>The first kick was an issue that my friend found in keybase, which was closed as informative.</p>
<p>The next day I decided to take a closer look at keybase because it was a 
chat application like discord but it was <strong><em>end2end encrypted</em></strong>.<br />
Keybase is free so there was no paywall blocking me neither any paid membership crap.
Since I had experience using chat apps like discord, messenger, etc.
I didn't have to learn the basics of how to use Keybase.</p>
<p>Firstly I thought little issues, little leaks could also have a great impact
because of the nature of end2end encryption.</p>
<p>And that was the time when me and the boys started looking into Keybase's webapp and desktop clients.</p>
<p>My daily driver is debian and that's the reason why I preferred to look into the linux app.
In the first days we didn't find anything. We just started using keybase
and started feeling how it works and what are the differences compared to other chat applications.</p>
<p>Then I started reading public writeups on keybase bugs published on <a href="https://hackerone.com/keybase/hacktivity">hackerone's hacktivity</a>.
Some of them were privilege escalations with the installer some of them were really weird bugs,
and I also found <a href="https://hackerone.com/reports/713006">windows path related issues</a>.</p>
<p>Oh boy, my favorite was when the tester gave the file a name with a full windows path and sent it in the chat to its victim.
When the victim downloaded it the file got stored on the location where the absolute windows path pointed to. (<a href="https://hackerone.com/reports/713006">713006</a>)</p>
<p>In the next couple of days I found a bypass for one of the public issues
but the report is not public yet but I'll definitely post it as soon as it 
gets disclosed.</p>
<p>After getting a bounty for that report I was like damn this was great,
it's time to go to parties with frieds and other stuff like that because I had the money for it. :D</p>
<p>After couple of weeks, I started poking keybase again on the weekends because I
didn't have anything else to do at the moment.
I thought if there was one issue I should be able to find another.
I just started thinking about possible issues in the next week or two without even touching the application.</p>
<p>And when the weekend came something popped into my mind.
What if I give a file a name with a traversal in it?
I mean a windows-only traversal with backslashes like ..\..\..\ becaues we
can name a file like this on linux (backlash \ is allowed, unlike forwardslash /)
then keybase syncronizes it to windows and you will have a file with a traversal in it's name.</p>
<p>I uploaded this file into a group and I traversed to an other file which was
inside of an other user's public directory.</p>
<p>I opened the location in windows explorer and tried to click on the file with
the traversal in in the name, but my windows 7 just errored out that the file was
inaccessible. Well fuck, it didn't work. Or maybe it did just on another windows OS?
That day I created and tried multiple traversal payloads with several apps like cmd and stuff
but none of them worked on my windows 7.
In the afternoon my friend just joined discord and <strong><em>asked why I posted a cmd icon image</em></strong> in the group.
I was like holy moly the traversal worked on his OS which was the <strong>most recent windows 10</strong> in this case.</p>
<p>We quickly tried it with the image you can see in my report (doge meme --trav--&gt; h1 wallpaper) and it also worked.
Then we recorded the POC with the wordfiles which also worked well.</p>
<p>After that I tried already known windows issues like the disk corruption,
and the one where I named the file/directory to an absolute windows path like
<code>C:\Users\asdf\Desktop</code> and when the victim tried to delete the file/directory
the actual path got deleted. (For instance, the user would've deleted his own desktop
folder in this example).</p>
<p>However, this did not work on windows 7 but it did on XP which means microsoft
<strong><em>probably</em></strong> developed windows 10 from an XP base and they completely threw
the windows vista/7 line away. This could mean you might be able to find some older
windows bugs on windows 10 which were patched in vista or 7.</p>
<h2>Timeline</h2>
<pre><code>Report SENT:
    April 18, 2021 16:56:07 CEST

- NTFS Disc Corruption (C:\:$i30:$bitmap) + File deletion with absolute paths:
    April 28, 2021 22:20:53 CEST
- Automatic NTFS Disc Corruption with web link icons:
    April 29, 2021 12:22:46 CEST

Report TRIAGED:
    June 7, 2021 22:31:31 CEST
Bounty:
    June 7, 2021 22:32:53 CEST

- Retest comment, (little UI issue after patch):
    June 11, 2021 16:01:02 CEST

Report RESOLVED:
    September 17, 2021 18:08:51 CEST
</code></pre>
<p>Original link: <a href="https://hackerone.com/reports/1167820">hackerone.com/reports/1167820</a></p>
</div>

</div>

<hr /><hr />
<footer>
	<p>Author: M4t35Z</p>
	<p><a href="https://www.buymeacoffee.com/m4t35z">Support the creator (buymeacoffee)</a></p>
	<p><i>Created with <a href="https://github.com/karlb/smu/">smu</a> + <a href="https://git.suckless.org/sites/">suckless style</a></i></p>
</footer>
</html>
